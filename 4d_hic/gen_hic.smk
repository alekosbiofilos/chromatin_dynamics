from snakemake import rules
from snakemake.io import glob_wildcards, expand, unpack
import re
from glob import glob
from pathlib import Path
import gzip

configfile: "config.yaml"

LIB1, LIB2, C1, C2, C3, C4, LANE, FR, EXTRA = glob_wildcards("fastq/{lib1}_{lib2}_{code1}_{code2}_{code3}_{code4}_NoIndex_L{lane}_R{fr}_{extra}.NoBarcode.fq.gz")

"""
HChi10004_HChi10007_170511_SN549_0241_BCANP9ACXX_NoIndex_L005_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_170810_SN549_0245_ACAWWPACXX_NoIndex_L003_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L002_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L003_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L004_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L005_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L006_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz

HChi10004_HChi10007_170511_SN549_0241_BCANP9ACXX_NoIndex_L005_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_170810_SN549_0245_ACAWWPACXX_NoIndex_L003_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L002_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L003_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L004_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L005_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L006_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz
"""

"""
HChi10005_HChi10008_170511_SN549_0241_BCANP9ACXX_NoIndex_L006_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_170810_SN549_0245_ACAWWPACXX_NoIndex_L004_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0249_ACAP75ACXX_NoIndex_L007_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0249_ACAP75ACXX_NoIndex_L008_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L001_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L002_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L003_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz

HChi10005_HChi10008_170511_SN549_0241_BCANP9ACXX_NoIndex_L006_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_170810_SN549_0245_ACAWWPACXX_NoIndex_L004_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0249_ACAP75ACXX_NoIndex_L007_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0249_ACAP75ACXX_NoIndex_L008_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L001_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L002_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L003_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz

"""

"""
HChi10006_HChi10009_170511_SN549_0241_BCANP9ACXX_NoIndex_L007_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_170810_SN549_0245_ACAWWPACXX_NoIndex_L005_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L004_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L005_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L006_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L007_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L008_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz

HChi10006_HChi10009_170511_SN549_0241_BCANP9ACXX_NoIndex_L007_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_170810_SN549_0245_ACAWWPACXX_NoIndex_L005_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L004_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L005_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L006_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L007_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L008_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz

"""

def input_fq(wildcards):
    reads1 = list(glob(f"fastq/{wildcards.lib1}_{wildcards.lib2}_*_*_*_{wildcards.code4}_NoIndex_L*_R1_*.NoBarcode.fq.gz"))
    reads2 = list(glob(f"fastq/{wildcards.lib1}_{wildcards.lib2}_*_*_*_{wildcards.code4}_NoIndex_L*_R2_*.NoBarcode.fq.gz"))
    return dict(reads1=reads1, reads2=reads2)


rule all:
    input:
        "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".sa",
        # expand("dedup_pairs/{lib1}_{lib2}.dedup.pairs.gz", zip, lib1=LIB1, lib2=LIB2),
        expand("pair_qc/{lib1}_{lib2}.zip", zip, lib1=LIB1, lib2=LIB2),
        "merged_pairs/merged.pairs.gz.px2"

rule index_genome:
    input: config["genome"]
    params:
        prefix = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name
    output:
        fa  = touch("bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name),
        mb  = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".amb",
        ann = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".ann",
        bwt = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".bwt",
        pac = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".pac",
        sa  = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".sa"
    shell: "bwa index {input} -p {params.prefix}"

rule merge_lanes:
    input: unpack(input_fq)
    output:
        fq1 = "fastq/merged/{lib1}_{lib2}_{code4}_R1.fq.gz",
        fq2 = "fastq/merged/{lib1}_{lib2}_{code4}_R2.fq.gz"

    shell: "cat {input.reads1} > {output.fq1}; cat {input.reads2} > {output.fq2}"


rule bwa:
    input:
        fq = rules.merge_lanes.output,
        bwa_ix= rules.index_genome.output.fa
    output:
        bam = "bam/{lib1}_{lib2}_{code4}.bam"
    threads: config["cpu"]
    shell:
        """
        bwa mem -t {threads} -SP5M {rules.index_genome.output.fa} {input.fq} | samtools sort - > bam/{wildcards.lib1}_{wildcards.lib2}_{wildcards.code4}.bam
        """

rule pairsam_parse_sort:
    input:
        bam = rules.bwa.output,
        chromsizes = config["chrom_sizes"]
    output: "pairs/{lib1}_{lib2}_{code4}.sam.pairs.gz"
    threads: config["cpu"]
    params:
        mem = config["mem"]
    shell:
        """
        mkdir -p /tmp/hic
        samtools view -h {input.bam} --threads {threads} | {{ 
        pairtools parse -c {input.chromsizes} --add-columns mapq 
        }} | {{ 
        pairtools sort --nproc {threads} --memory {params.mem}G --compress-program lz4c --tmpdir /tmp/hic --output {output}
        }}
        rm -r /tmp/hic
        """


def input_pairsam_merge(wc):
    return set([f"pairs/{wc.lib1}_{wc.lib2}_{c4}.sam.pairs.gz"
            for l1, l2, c4 in zip(LIB1, LIB2, C4)
            if l1 == wc.lib1 and l2 == wc.lib2])

def n_pairs(wc): return len(input_pairsam_merge(wc))

rule pairsam_merge:
    input:
        pairfiles = input_pairsam_merge
    output: "pairs_merged/{lib1}_{lib2}.sam.pairs.gz"
    threads: config["cpu"]
    params:
        mem = config["mem"],
        n_files = n_pairs
    shell:
        """
        pairtools merge --max-nmerge {params.n_files} --nproc {threads} --memory {params.mem} --output {output} {input.pairfiles}
        """


rule pairsam_markdups:
    input: "pairs_merged/{lib1}_{lib2}.sam.pairs.gz"
    output: "pairs_merged/{lib1}_{lib2}.marked.sam.pairs"
    shell:
        """
        pairtools dedup --mark-dups --output-dups - --output-unmapped - --output {output} {input} \
        pairix {output}
        """

rule pairsam_filter:
    input:
        pair = rules.pairsam_markdups.output,
        chromsizes = config["chrom_sizes"]
    output:
        unmapped = "dedup_pairs/{lib1}_{lib2}.unmapped.sam.pairs.gz",
        dedup_pairs = "dedup_pairs/{lib1}_{lib2}.dedup.pairs.gz",
        lossless_bam = "dedup_pairs/{lib1}_{lib2}.lossless.bam",
        tempfile = "dedup_pairs/{lib1}_{lib2}.temp.gz",
        tempfile1 = "dedup_pairs/{lib1}_{lib2}.temp1.gz"
    shell:
        """
        pairtools split --output-sam {output.lossless_bam} {input.pair} \ 
        # Select UU, UR, RU reads
        pairtools select '(pair_type == "UU") or (pair_type == "UR") or (pair_type == "RU")' \ 
        --output-rest {output.unmapped} --output {output.tempfile} {input.pair} \ 
        
        pairtools split --output-pairs {output.tempfile1} {output.tempfile} \ 
        
        pairtools select 'True' --chrom-subset {input.chromsizes} -o {output.dedup_pairs} {output.tempfile1} \ 
        pairix {output.dedup_pairs}
        """

rule pair_qc_single:
    input: rules.pairsam_filter.output.dedup_pairs
    params:
        sample_name = "{lib1}_{lib2}",
        out_dir = "pair_qc",
        chromsizes = config["chrom_sizes"],
        # enzyme site length
        enzyme = 4,
        max_distance = 8.4,
        py_code = config["pairqc"],
        r_code = config["rplot"]
    output: "pair_qc/{lib1}_{lib2}.zip"
    shell:
        """
        {params.py_code} -p {input} -c {params.chromsizes} -tP -s {params.sample_name} -O {params.sample_name} -M {params.max_distance} \ 
        {params.r_code} {params.enzyme} {params.sample_name}/report \ 
        zip -r pair_qc/{params.sample_name}.zip {params.sample_name}
        """

rule pairs_merge:
    input: expand("dedup_pairs/{lib1}_{lib2}.dedup.pairs.gz", zip, lib1=LIB1, lib2=LIB2)
    output:
        body    = "merged_pairs/merged.pairs_body.txt",
        pairs   = "merged_pairs/merged.pairs",
        gzipped = "merged_pairs/merged.pairs.gz",
        ix      = "merged_pairs/merged.pairs.gz.px2"
    run:
        commands = ""
        body = ""
        out = Path(output.pairs)
        body_out = Path(output.body)
        with out.open("w") as o, body_out.open("w") as b_o:
            for ix, f in enumerate(input):
                with gzip.open(str(f), "rt") as lines:
                    for line in lines:
                        if line.startswith("#"):
                            if line.startswith("#command"):
                                commands += line
                            # Only write comment lines from first file
                            elif ix == 0:
                                o.write(line)
                        else:
                            b_o.write(line)
            o.write(commands)
        # sort
        shell("""
        sort -m -k2,2 -k4,4 -k3,3g -k5,5g {output.body} >> {output.pairs} \ 
        bgzip -f {output.pairs} \ 
        pairix -f {output}
        """)

# To make cooler: cooler cload pairix -p $ncores -s $max_split chromSizes:$bin_size $pairs_file $out_prefix.cool
