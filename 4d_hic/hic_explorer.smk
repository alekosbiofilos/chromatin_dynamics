from snakemake import rules
from snakemake.io import glob_wildcards, expand, unpack
import re
from glob import glob
from pathlib import Path
import gzip

configfile: "config.yaml"

LIB1, LIB2, C1, C2, C3, C4, LANE, FR, EXTRA = glob_wildcards("fastq/{lib1}_{lib2}_{code1}_{code2}_{code3}_{code4}_NoIndex_L{lane}_R{fr}_{extra}.NoBarcode.fq.gz")

"""
HChi10004_HChi10007_170511_SN549_0241_BCANP9ACXX_NoIndex_L005_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_170810_SN549_0245_ACAWWPACXX_NoIndex_L003_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L002_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L003_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L004_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L005_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L006_R1_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz

HChi10004_HChi10007_170511_SN549_0241_BCANP9ACXX_NoIndex_L005_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_170810_SN549_0245_ACAWWPACXX_NoIndex_L003_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L002_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L003_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L004_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L005_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz 
HChi10004_HChi10007_171221_SN549_0249_ACAP75ACXX_NoIndex_L006_R2_001.F6-002-HIC-000-00001-1591.NoBarcode.fq.gz
"""

"""
HChi10005_HChi10008_170511_SN549_0241_BCANP9ACXX_NoIndex_L006_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_170810_SN549_0245_ACAWWPACXX_NoIndex_L004_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0249_ACAP75ACXX_NoIndex_L007_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0249_ACAP75ACXX_NoIndex_L008_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L001_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L002_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L003_R1_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz

HChi10005_HChi10008_170511_SN549_0241_BCANP9ACXX_NoIndex_L006_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_170810_SN549_0245_ACAWWPACXX_NoIndex_L004_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0249_ACAP75ACXX_NoIndex_L007_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0249_ACAP75ACXX_NoIndex_L008_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L001_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L002_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz 
HChi10005_HChi10008_171221_SN549_0250_BCAY69ACXX_NoIndex_L003_R2_001.F6-002-HIC-000-00002-8B42.NoBarcode.fq.gz

"""

"""
HChi10006_HChi10009_170511_SN549_0241_BCANP9ACXX_NoIndex_L007_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_170810_SN549_0245_ACAWWPACXX_NoIndex_L005_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L004_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L005_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L006_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L007_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L008_R1_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz

HChi10006_HChi10009_170511_SN549_0241_BCANP9ACXX_NoIndex_L007_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_170810_SN549_0245_ACAWWPACXX_NoIndex_L005_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L004_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L005_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L006_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L007_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz 
HChi10006_HChi10009_171221_SN549_0250_BCAY69ACXX_NoIndex_L008_R2_001.F6-002-HIC-000-00003-B29C.NoBarcode.fq.gz

"""

def input_fq(wildcards):
    reads1 = list(glob(f"fastq/{wildcards.lib1}_{wildcards.lib2}_*_*_*_{wildcards.code4}_NoIndex_L*_R1_*.NoBarcode.fq.gz"))
    reads2 = list(glob(f"fastq/{wildcards.lib1}_{wildcards.lib2}_*_*_*_{wildcards.code4}_NoIndex_L*_R2_*.NoBarcode.fq.gz"))
    return dict(reads1=reads1, reads2=reads2)


rule all:
    input:
        "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".sa",
        expand("matrix/{lib1}_{lib2}_{code4}.h5", zip, lib1=LIB1, lib2=LIB2, code4=C4)

rule index_genome:
    input: config["genome"]
    params:
        prefix = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name
    output:
        fa  = touch("bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name),
        mb  = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".amb",
        ann = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".ann",
        bwt = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".bwt",
        pac = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".pac",
        sa  = "bwa_ix/" + Path(f"bwa_ix/{config['genome']}").name + ".sa"
    shell: "bwa index {input} -p {params.prefix}"

rule merge_lanes:
    input: unpack(input_fq)
    output:
        fq1 = "fastq/merged/{lib1}_{lib2}_{code4}_R1.fq.gz",
        fq2 = "fastq/merged/{lib1}_{lib2}_{code4}_R2.fq.gz"

    shell: "cat {input.reads1} > {output.fq1}; cat {input.reads2} > {output.fq2}"


rule bwa:
    input:
        fq1 = rules.merge_lanes.output.fq1,
        fq2 = rules.merge_lanes.output.fq2,
        bwa_ix= rules.index_genome.output.fa
    output:
        bam1 = "bam/{lib1}_{lib2}_{code4}_1.bam",
        bam2 = "bam/{lib1}_{lib2}_{code4}_2.bam"
    threads: config["cpu"]
    shell:
        """
        bwa mem -t {threads} -A 1 -B 4 -E 50 -L 0 {rules.index_genome.output.fa} {input.fq1} | samtools view -Shb - > {output.bam1}
        bwa mem -t {threads} -A 1 -B 4 -E 50 -L 0 {rules.index_genome.output.fa} {input.fq2} | samtools view -Shb - > {output.bam2}
        """

rule rest_sites:
    input: config["genome"]
    output: "hiexp/rest_sites.bed"

    params:
        site = config["restriction_site"]
    conda: "condas/hicexplorer.yaml"
    shell:
        "hicFindRestSite -f {input} -p {params.site} -o {output}"

rule build_matrix:
    input:
        bam = rules.bwa.output,
        rest_sites = rules.rest_sites.output
    output:
        matrix = "matrix/{lib1}_{lib2}_{code4}.h5",
        bam = "bam_matrix/{lib1}_{lib2}_{code4}.bam"
    conda: "condas/hicexplorer.yaml"
    threads: config["cpu"]
    params:
        min_res = config["highest_resolution"],
        rest_site = config["restriction_site"]
    shell:
        """
        hicBuildMatrix --samFiles {input.bam} \
        --binSize {params.min_res} \
        --restrictionSequence {params.rest_site} \
        --danglingSequence {params.rest_site} \
        --restrictionCutFile {input.rest_sites} \
        --threads {threads} \
        --inputBufferSize 100000 \
        --outBam {output.bam} \
        -o {output.matrix} \
        --QCfolder ./hicQC
        """


rule merge_matrices:
    input: expand("matrix/{lib1}_{lib2}_{code4}.h5", zip, lib1=LIB1, lib2=LIB2, code4=C4)
    output: "f6.h5"
    conda: "condas/hicexplorer.yaml"
    shell:
        "hicSumMatrices --matrices {input} --outFileName {output}"
