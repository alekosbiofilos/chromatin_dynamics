# Parsers
Here we will find parser code for performing diverse analyses of Hi-C-related files in different formats

## hic_intersect.py
Intersect Hi-C files from the [3D genome interaction viewer from KAIST](http://3div.kr/), against a peak file from [Cicero](https://cole-trapnell-lab.github.io/cicero-release/docs/), an R module for the analysis of single-cell chromatin accessibilty experiments.  
Basically, this script converts both the Cicero output and the Hi-C files to bedpe format, and overlaps them, returning contacts that also include pairs from Cicero.