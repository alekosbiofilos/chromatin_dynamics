from pathlib import Path
import gzip
from sys import argv
from subprocess import run
import shlex

"""
Usage: python hic_intersect.py peak_file hic_dir resolution out_bedpe
Note: the Hi-C data has to be gzipped and with the format
idx	chr	bin1	bin2	RawCount	RawCount	CapturabilityOne	CapturabilityTwo	Distance	all_exp	all_capture_res	exp_value_dist	dist_foldchange	rescaled_intensity

IMPORTANT: It will report the interaction frequency from the "rescaled_intensity" 
column
NOTE: Some files might be tag files or not compressed correctly. Files with names starting with "._" will be ignored
"""

peaks = Path(argv[1])
hic = Path(argv[2])
resolution = int(argv[3])
out_bedpe = Path(argv[4])

def peaks_to_bed(peaks):
    """
    Parses file of the format: chr1-start1-end1 chr2-start2-end2
    as a bedpe file, and returns the name of the generated file
    """
    peaks_bed = Path(str(peaks) + ".bedpe")
    with peaks_bed.open("w") as o:
        for line in peaks.open():
            if "Peak" not in line:
                coord1, coord2 = line.split()
                chr1, start1, end1 = coord1.split("-")
                chr2, start2, end2 = coord2.split("-")
                o.write("\t".join((chr1, start1, end1, chr2, start2, end2)) + "\n")
    return peaks_bed

def hic_data_to_bedpe(data, res=resolution):
    print(f"Uncompressing {data}")
    hic_bedpe = Path(data.stem + ".bedpe")
    str_out = ""
    try:
        with gzip.open(str(data), "rt") as lines:
            print(f"Parsing {data}")
            for line_data in lines:
                if "idx" not in line_data:
                    fields = line_data.split()
                    chrom, bin1, bin2 = fields[1:4]
                    freq = fields[-1]
                    start1 = int(bin1)
                    end1 = start1 + res
                    start2 = int(bin2)
                    end2 = start2 + res
                    bedpe_lst = (chrom, str(start1), str(end1),
                                chrom, str(start2), str(end2), ".", freq)
                    str_out += "\t".join(bedpe_lst) + "\n"
        return str_out
    except:
        print(f"{str(data)} failed")
        

# 1. convert peaks file into bedpe format

# Parse peaks to bedpe, and return the name of the generated file
print("Generating peaks bedpe file")
peaks_bed = peaks_to_bed(peaks)

# 2. Iterate over the hi-c files, and run the intersection on each one
# Open output file
with out_bedpe.open("w") as out:
    if not "._" in hic.stem:
        # Generate bedpe string from hi-c format
        hic_bed = hic_data_to_bedpe(hic)
        # Build command
        cmd = shlex.split(f"pairToPair -a {peaks_bed} -b stdin")
        # Run command
        print(f"Intersecting {peaks_bed} and {hic}")
        bed_intersect = run(cmd, text=True, stdout=out, input=hic_bed)
