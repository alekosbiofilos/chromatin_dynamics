# Chromatin dynamics

Pipelines and misc scripts related to comparative chromatin conformation analyses

## 4d_hic
Snakemake implementation of the [4D Nucleome Hi-C pipeline](https://github.com/4dn-dcic/docker-4dn-hic).

## parsers
Scripts for routine parsing and basic operations with Hi-C-related files.